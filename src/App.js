
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import User from './Components/User'
import pic1 from './Components/pics/111.png'
import pic2 from './Components/pics/222.png'
import pic3 from './Components/pics/333.png'


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      //assigning intial records to show on list
      users: [
        {
          id: "001",
          firstName: "Will",
          lastName: "Smith",
          address: "100 Gampola,Kandy",
          contact: "+94 0134654562",
          dob: " 12 / 01 / 1995",
          email: "will@gmail.com",
          image: pic1
        },
        {
          id: "002",
          firstName: "Leonel",
          lastName: "Messi",
          address: "100 Barcelona,Spain",
          contact: "+90 1254867453",
          dob: "04 / 01 / 1985",
          email: "leo123@gmail.com",
          image: pic2
        },
        {
          id: "003",
          firstName: "Jack",
          lastName: "Ma",
          address: "100 MainStreet,Chaina",
          contact: "+96 46879876778",
          dob: "20 / 08 / 1975",
          email: "jackma@yahoo.com",
          image: pic3
        }

      ],
      act: "Save",
      index: 0,
      file: null
    }

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.setState({
      file: URL.createObjectURL(event.target.files[0])
    })
  }

  deleteUser = (index, e) => { //delete user function
    const users = Object.assign([], this.state.users)
    users.splice(index, 1);
    this.setState({ users: users })
  }

  SaveOrUpdate = () => { //save or update user function

    let tmpUsers = this.state.users;


    let id = "004";
    let image = this.state.file;
    let firstName = this.refs.firstName.value;
    let lastName = this.refs.lastName.value;
    let address = this.refs.address.value;
    let contact = this.refs.contact.value;
    let dob = this.refs.dob.value;
    let email = this.refs.email.value;

    if (this.state.act === "Save") {
      let tmpuser = { id, image, firstName, lastName, address, contact, dob, email }

      tmpUsers.push(tmpuser);
    }
    else {
      let index = this.state.index;

      tmpUsers[index].id = id;
      tmpUsers[index].pic = image;
      tmpUsers[index].firstName = firstName;
      tmpUsers[index].lastName = lastName;
      tmpUsers[index].address = address;
      tmpUsers[index].contact = contact;
      tmpUsers[index].dob = dob;
      tmpUsers[index].email = email;
    }

    this.setState({
      users: tmpUsers,
      act: "Save",
      file: null
    });
    this.refs.CreateForm.reset();

  }

  editUser = (index) => { //to use edit existing records
    let tmpuser = this.state.users[index];

    this.state.file = tmpuser.image;
    this.refs.firstName.value = tmpuser.firstName;
    this.refs.lastName.value = tmpuser.lastName;
    this.refs.address.value = tmpuser.address;
    this.refs.contact.value = tmpuser.contact;
    this.refs.dob.value = tmpuser.dob;
    this.refs.email.value = tmpuser.email;
    this.setState({
      act: "Update",
      index: index
    });

  }

  InitializeForm = (props) => {
    return (
      <div>
        <br />
        <br />
        <br />
        <br />
        <span>
        <form  ref="CreateForm" padding="10px"> 
          
          <input ref="firstName" type="text" placeholder="First Name" />
          <input ref="lastName" type="text" placeholder="Last Name" />
          <input ref="address" type="text" placeholder="Address" />
          <input ref="contact" type="text" placeholder="Contact" />
          <input ref="dob" type="date" />
          <input ref="email" type="email" placeholder="Email" />
          
          <input ref="pic" type="file" accept="image/x-png,image/gif,image/jpeg"  onChange={this.handleChange} />
          <br/>
          <img src={this.state.file} ref="tempImage" width="50" height="50" />
        </form>
           
          <br/>
          <button ref="save" onClick={this.SaveOrUpdate}>{this.state.act}</button>
          

        </span>
        <br />
        <br />
      </div>
    )
  }

  render() {
    return (
      <div className="App">
        {this.InitializeForm()}
        <table className="table">
          <tbody>
            <tr>
              <th></th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Address</th>
              <th>Contact</th>
              <th>Date Of Birth</th>
              <th>Email</th>
              <th></th>
              <th></th>
            </tr>
            {
              this.state.users.map((user, index) => {
                return (
                  <User id={user.id}
                    pic={user.image}
                    key={user.id}
                    firstName={user.firstName}
                    lastName={user.lastName}
                    address={user.address}
                    contact={user.contact}
                    dob={user.dob}
                    email={user.email}
                    deleteMethod={this.deleteUser.bind(this, index)}
                    editMethod={this.editUser.bind(this, index)}>
                  </User>
                )
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

export default App;
