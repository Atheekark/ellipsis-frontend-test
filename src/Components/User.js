import React from 'react';

const user = (props) => {
    return (<tr>
        <td><img src={props.pic} width="50" height ="50"></img></td>
        <td>{props.firstName}</td>
        <td>{props.lastName}</td>
        <td>{props.address}</td>
        <td>{props.contact}</td>
        <td>{props.dob}</td>
        <td>{props.email}</td>
        <td><button onClick={props.editMethod}>Edit</button></td>
        <td><button onClick={props.deleteMethod}>Delete</button></td>
    </tr>)
}

export default user;